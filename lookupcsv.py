import sys
import difflib
import csv
import pandas as pd

#Import csv file into pandas dataframe
df = pd.read_csv(sys.argv[1])
#Name the different columns in the file and convert them to lower case/string
Master = df.Master.str.lower() #.astype(str).values.tolist()
MappedID = df.Mapped.astype(str).str.lower()
#Use difflib.get_close_matches to lookup MappedID in a giant column of known names
df['Name_r'] = MappedID.map(lambda x: (difflib.get_close_matches(x, Master, cutoff=0.8)[:1] or [None][0]))
header = [ "Master", "MappedID", "Name_r"] 
#Write output to csv
df.to_csv(sys.argv[2], columns = header, quoting=csv.QUOTE_NONE)
print(df.to_string())